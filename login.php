<?php
/**
 * Created by PhpStorm.
 * User: viraj
 * Date: 5/12/2016
 * Time: 10:33
 */

if (isset($_POST['LoginForm']) && !empty($_POST['LoginForm']['username']) && !empty($_POST['LoginForm']['password'])){

    $username = $_POST['LoginForm']['username'];
    $password = $_POST['LoginForm']['password'];

    $users = simplexml_load_file('users.xml');

    $found = false;
    foreach($users as $user){

        if(($user->username == $username) && ($user->password == $password)){

            $found = true;
            session_start();
            $_SESSION['username'] = $username;

            header("Location: dashboard.php");

        }

    }

    if(!$found){
        header("Location: index.php");
    }

}else{

    header("Location: index.php");
    exit();

}
