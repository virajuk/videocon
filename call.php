<?php
/**
 * Created by PhpStorm.
 * User: viraj
 * Date: 5/13/2016
 * Time: 10:48
 */
?>

<?php
session_start();
if(!isset($_SESSION['username'])){
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Videocon | call</title>

        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="assets/css/custom.css" rel="stylesheet">

        <!-- CSS -->
        <link rel="stylesheet" href="assets/css/styles.css">

    </head>
    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                <?php include 'layouts/side-bar.php' ;?>

                <?php include 'layouts/header.php' ;?>

                <div class="main-container">

                    <div class="video-container">
                        <div id="videoHolderSmall" class="video-wrap primary-video video-background"></div>
                        <div class="local-controls">
                            <div class="video-control circle video enabled hidden" id="enableLocalVideo"></div>
                            <div class="video-control circle call" id="callActive"></div>
                            <div class="video-control circle microphone enabled hidden" id="enableLocalAudio"></div>
                        </div>
                        <div id="videoHolderBig" class="video-wrap secondary-video"></div>
                        <div id="remoteControls" class="remote-controls hidden">
                            <div class="video-control audio" id="enableRemoteAudio"></div>
                            <div class="video-control video" id="enableRemoteVideo"></div>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        <script src="https://static.opentok.com/v2/js/opentok.min.js" type="text/javascript" defer></script>
        <!-- jQuery -->
        <script src="vendors/jquery/dist/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js" defer></script>
        <script src="assets/js/components/one-to-one-communication.js" type="text/javascript" defer></script>
        <script src="assets/js/components/otkanalytics.js" type="text/javascript" defer></script>
        <script src="assets/js/app.js" type="text/javascript" defer></script>
        <!-- Bootstrap -->
        <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Flot -->
        <script src="vendors/Flot/jquery.flot.js"></script>
        <script src="vendors/Flot/jquery.flot.pie.js"></script>
        <script src="vendors/Flot/jquery.flot.time.js"></script>
        <script src="vendors/Flot/jquery.flot.stack.js"></script>
        <script src="vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="assets/js/flot/jquery.flot.orderBars.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="assets/js/custom.js"></script>

    </body>
</html>
