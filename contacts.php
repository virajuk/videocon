<?php
/**
 * Created by PhpStorm.
 * User: viraj
 * Date: 5/12/2016
 * Time: 18:06
 */
?>

<?php
session_start();
if(!isset($_SESSION['username'])){
    header("Location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Videocon | Contact</title>

        <!-- Bootstrap -->
        <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <!-- iCheck -->
        <link href="vendors/iCheck/skins/flat/green.css" rel="stylesheet">
        <!-- bootstrap-progressbar -->
        <link href="vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
        <!-- jVectorMap -->
        <link href="assets/css/maps/jquery-jvectormap-2.0.3.css" rel="stylesheet"/>

        <!-- Custom Theme Style -->
        <link href="assets/css/custom.css" rel="stylesheet">
    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">

                <?php include 'layouts/side-bar.php' ?>

                <?php include 'layouts/header.php' ;?>

                <!-- page content -->
                <div class="right_col" role="main">

                    <div class="">
                        <div class="page-title">
                            <div class="title_left">
                                <h3>Contacts</h3>
                            </div>

                            <!--<div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                          <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>-->
                        </div>

                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="x_panel">
                                    <div class="x_content">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12 text-center">
                                                <!--<ul class="pagination pagination-split">
                                                    <li><a href="#">A</a></li>
                                                    <li><a href="#">B</a></li>
                                                    <li><a href="#">C</a></li>
                                                    <li><a href="#">D</a></li>
                                                    <li><a href="#">E</a></li>
                                                    <li>...</li>
                                                    <li><a href="#">W</a></li>
                                                    <li><a href="#">X</a></li>
                                                    <li><a href="#">Y</a></li>
                                                    <li><a href="#">Z</a></li>
                                                </ul>-->
                                            </div>

                                            <div class="clearfix"></div>

                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Travel Consultant</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Dushmantha</h2>
                                                            <p><strong>About: </strong> Experienced travel guide in SriLanka </p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>4.0</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <a href="call.php" type="button" class="btn btn-success btn-xs">
                                                                <i class="fa fa-user"></i>
                                                                <i class="fa fa-video-camera"></i>
                                                            </a>
                                                            <!--<button type="button" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-user"> </i> View Profile
                                                            </button>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Traveller</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Test User</h2>
                                                            <p><strong>About: </strong> Web Designer / UI. </p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>3.0</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <a href="call.php" type="button" class="btn btn-success btn-xs">
                                                                <i class="fa fa-user">
                                                                </i> <i class="fa fa-video-camera"></i>
                                                            </a>
                                                            <!--<button type="button" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-user"> </i> View Profile
                                                            </button>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Travel Agent</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Andrew Wilson</h2>
                                                            <p><strong>About: </strong>Travel Agent</p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>1.5</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-half-o"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <a href="call.php" type="button" class="btn btn-success btn-xs">
                                                                <i class="fa fa-user">
                                                                </i> <i class="fa fa-video-camera"></i>
                                                            </a>
                                                            <!--<button type="button" class="btn btn-primary btn-xs">
                                                                <i class="fa fa-user"> </i> View Profile
                                                            </button>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <!--<div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Digital Strategist</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Nicole Pearson</h2>
                                                            <p><strong>About: </strong> Web Designer / UI. </p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>4.0</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                                                </i> <i class="fa fa-video-camera"></i> </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Digital Strategist</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Nicole Pearson</h2>
                                                            <p><strong>About: </strong> Web Designer / UI. </p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>4.0</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                                                </i> <i class="fa fa-video-camera"></i> </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                                <div class="well profile_view">
                                                    <div class="col-sm-12">
                                                        <h4 class="brief"><i>Digital Strategist</i></h4>
                                                        <div class="left col-xs-7">
                                                            <h2>Nicole Pearson</h2>
                                                            <p><strong>About: </strong> Web Designer / UI. </p>
                                                            <ul class="list-unstyled">
                                                                <li><i class="fa fa-building"></i> Address: N/A</li>
                                                                <li><i class="fa fa-phone"></i> Phone #: N/A</li>
                                                            </ul>
                                                        </div>
                                                        <div class="right col-xs-5 text-center">
                                                            <img src="assets/images/user.png" alt="" class="img-circle img-responsive">
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 bottom text-center">
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <p class="ratings">
                                                                <a>4.0</a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star"></span></a>
                                                                <a href="#"><span class="fa fa-star-o"></span></a>
                                                            </p>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 emphasis">
                                                            <button type="button" class="btn btn-success btn-xs"> <i class="fa fa-user">
                                                                </i> <i class="fa fa-video-camera"></i> </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>-->

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /page content -->


            </div>
        </div>
        <!-- jQuery -->
        <script src="vendors/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Flot -->
        <script src="vendors/Flot/jquery.flot.js"></script>
        <script src="vendors/Flot/jquery.flot.pie.js"></script>
        <script src="vendors/Flot/jquery.flot.time.js"></script>
        <script src="vendors/Flot/jquery.flot.stack.js"></script>
        <script src="vendors/Flot/jquery.flot.resize.js"></script>
        <!-- Flot plugins -->
        <script src="assets/js/flot/jquery.flot.orderBars.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="assets/js/custom.js"></script>
    </body>
</html>