<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Videocon</title>

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="assets/css/custom.css" rel="stylesheet">
</head>
<body style="background:#F7F7F7;">
    <div class="">
        <a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

        <div id="wrapper">

            <div id="login" class=" form">
                <section class="login_content">
                    <form method="post" action="login.php">
                        <h1>Login Form</h1>
                        <div>
                            <input type="text" class="form-control" placeholder="Username" name="LoginForm[username]" required="" />
                        </div>
                        <div>
                            <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]" required="" />
                        </div>
                        <div>
                            <input style="margin-left:40%;" type="submit" class="btn btn-default submit" name="LoginForm[login]" value="Log In" />
                            <!--<input type="submit" class="btn btn-default submit" value="Log In"/>-->
                            <!--<a class="btn btn-default submit" href="index.html">Log in</a>-->
                        </div>
                        <div class="clearfix"></div>
                        <div class="separator">


                        </div>
                    </form>
                </section>
            </div>

        </div>
    </div>
</body>
</html>
